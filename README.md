isoladora
==============

folders
- caja, the design of the encosing box
- circuito, the circuit and pcb design
- [sla](sla/README.md), the staff to make anycubic photon sla printer to isolate the pcb 

links:
- pcb & circuit design: https://easyeda.com/eccnil/isoladora
- case design: https://a360.co/2FG9ahf
