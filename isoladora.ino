
#define switchPin 3
#define buzzerPin 4
#define latchPin 1  //Pin connected to ST_CP of 74HC595
#define clockPin 2  //Pin connected to SH_CP of 74HC595
#define dataPin  0  //Pin connected to DS of 74HC595

#define step 60000  //time unit in ms
#define fullSteps 1000 //number used to tell the system is untimed

#define LA (1<<4) //Q4
#define LB (1<<7) //Q7
#define LC (1<<1) //Q1
#define LD (1<<3) //Q3
#define LE (1<<2) //Q2
#define LF (1<<5) //Q5
#define LG (1<<6) //Q6
#define LDP (1<<0) //Q0
#define D1 ( LB + LC )
#define D2 ( LA + LB + LE + LD + LG)
#define D3 ( LA + LB + LC + LD + LG)
#define D4 ( LF + LB + LG + LC )
#define D5 ( LA + LF + LG + LC + LD )
#define D6 ( LA + LF + LE + LD + LC + LG )
#define D7 ( LA + LB + LC  )
#define D8 ( LA + LB + LC + LD + LE + LF + LG  )
#define D9 ( LA + LB + LC + LF + LG )
#define D0 ( LA + LB + LC + LD + LE + LF  )
#define DF ( LA + LE + LF + LG )
#define D 0

const byte displayNumbers[10] = {D0,D1,D2,D3,D4,D5,D6,D7,D8,D9};

int click, released;
int stepsLeft;
unsigned long now, startingTime;
int shortBeep;
int numberDisplaying;

void setup()
{
  pinMode(buzzerPin, INPUT);
  pinMode(switchPin, INPUT_PULLUP);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  stepsLeft = 0;
  released = true;
  shortBeep = false;
  numberDisplaying = -1;
  click=false;
}

void clickBeepf(int *click, int steps){
  if(*click && steps){
    tone(buzzerPin, 2000 );
    delay( 200); 
    noTone(buzzerPin); 
  } 
}

void cancelBeepf(int *click, int steps){
  if(*click && !steps){
    tone(buzzerPin, 2000 ); delay(200); 
    noTone(buzzerPin); delay(50); 
    tone(buzzerPin, 1800 ); delay(200); 
    noTone(buzzerPin); delay(50); 
    tone(buzzerPin, 2000 ); delay(400); 
    noTone(buzzerPin); 
  } 
}

void shortBeepf(const int signal, const int stepsLeft){
   if(signal){
     if (stepsLeft){
        tone(buzzerPin, 2600 );
        delay(100);
        noTone(buzzerPin);
    }
  } 
}

void endBeepf(const int signal, const int stepsLeft){
  if(signal){
    if (stepsLeft == 0){
      const int melodyLenght = 5;
      const int melody[] = {1568, 1760, 1398, 1047, 1398};
      //const int melodyLenght = 4;
      //const int melody[] = { 1319, 1975, 1319, 1568};
      const int noteLenght = 320;
      const int noteSpace = 20;
      for(int j = 0; j < 1; j++){
        for(int i = 0; i < melodyLenght; i++){
          tone(buzzerPin, melody[i] );
          delay(noteLenght);
          noTone(buzzerPin);
          delay(noteSpace);
       }
      }
    }
  }
}

void actionClick(int *counter){
  if(*counter <9){
    (*counter) ++;
  } else if (*counter == fullSteps){
    *counter = 0;
  } else {
    *counter = fullSteps;
  }
}

void button(unsigned int now, int *click, int *released, int *counter){
  bool actionclick = false;
  *click = digitalRead(switchPin) == LOW;
  if(*click){
    if (released != true){
      *released = true;
      actionClick(counter);
    }
  } else {
    *released = true;
  }
}

void countDown (unsigned long now, unsigned long * to, int*counter, int * changed){
  *changed = false;
  if(*counter > 0 && *counter != fullSteps){
    if (*to < now){
      -- (*counter);
      *to = now + step;
      *changed = true;
    } 
  } else {
    *to = now + step;
  }
}

void segments7(const int numberToDisplay, int *numberDisplaying){
  //TODO solo hacer esto si el numero cambia
  if(*numberDisplaying != numberToDisplay ){
    //low latch
    digitalWrite(latchPin, LOW);
    //wirte number
    shiftOut(dataPin, clockPin, MSBFIRST, numberToDisplay );
    //latch out
    digitalWrite(latchPin, HIGH);
    //update numberDisplaying
    *numberDisplaying = numberToDisplay;
  }
}

int displayBits(const int stepsLeft){
  int result;
  if(stepsLeft >= 0 && stepsLeft <= 9 ){
    result = displayNumbers[stepsLeft]; 
  } else if (stepsLeft > 9) {
    result = DF;
  } else {
    return D;
  }
  if (stepsLeft == 0 ){
    result += LDP;
  }

  return ~result; //return "not" because is common anode
}

void loop()
{
  //TODO: longClick para parar la isolacion
  //TODO: musiquita para el final
  now = millis();
  button(now, &click, &released, &stepsLeft);
  countDown(now, &startingTime, &stepsLeft, &shortBeep);
  segments7(displayBits(stepsLeft), &numberDisplaying);
  clickBeepf(&click, stepsLeft);
  cancelBeepf(&click, stepsLeft);
  shortBeepf(shortBeep, stepsLeft);
  endBeepf(shortBeep, stepsLeft);
}